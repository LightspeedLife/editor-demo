.POSIX:
.SUFFIXES:

PLATFORM = linux
TARGET_NAME = play
TARGET_PLATFORM = TARGET_$(PLATFORM)
TARGET_EXT_linux =
TARGET_EXT_windows = .exe
TARGET_EXT = $(TARGET_EXT_$(PLATFORM))
TARGET_PATH = bin

# # Here was my idea for a completely neutral default target, but even after getting it to work, it was just too cumbersome.
# all: $(TARGET_PLATFORM)
# TARGET_none:
# 	@echo name target platform with PLATFORM=target
# 	@echo e.g. TARGET=windows
# TARGET_linux:
# 	make $(TARGET_PATH)/$(TARGET_NAME)$(TARGET_EXT) PLATFORM=linux
# TARGET_windows:
# 	make $(TARGET_PATH)/$(TARGET_NAME)$(TARGET_EXT) PLATFORM=windows

all: $(TARGET_PATH)/$(TARGET_NAME)$(TARGET_EXT)


COMPILER_linux = gcc
COMPILER_windows = x86_64-w64-mingw32-gcc
CC = $(COMPILER_$(PLATFORM))
LIBS = glfw3
CFLAGS_X_PLATFORM = -Wall -Wpedantic -std=c99 -Iinclude -Iinclude/s7 $(DEBUG_$(DEBUG))
DEBUG = 0
DEBUG_0 =
DEBUG_1 = -g
CFLAGS_linux =
CFLAGS_windows =
LDFLAGS_X_PLATFORM =
LDFLAGS_linux =
LDFLAGS_windows = -mwindows -Llib
LDLIBS_X_PLATFORM =
LDLIBS_linux = -lGL -lX11 -lglfw -lrt -lm -ldl -lX11 -lpthread -lxcb -lXau -lXdmcp
LDLIBS_windows = -lwinmm -lopengl32 -lwinpthread

CFLAGS = $(CFLAGS_X_PLATFORM) $(CFLAGS_$(PLATFORM))
LDFLAGS = $(LDFLAGS_X_PLATFORM) $(LDFLAGS_$(PLATFORM))
LDLIBS = $(LDLIBS_X_PLATFORM) $(LDLIBS_$(PLATFORM))

PLATFORM_OBJ_EXT_linux = .o
PLATFORM_OBJ_EXT_windows = .obj
PLATFORM_OBJ_EXT = $(PLATFORM_OBJ_EXT_$(PLATFORM))

.SUFFIXES: .c $(PLATFORM_OBJ_EXT)
.c.o:
	$(CC) $(CFLAGS) -c $< -o $@
.c.obj:
	$(CC) $(CFLAGS) -c $< -o $@
.c.h:

SOURCES = main.c s7_glfw.c s7_nuklear.c buffer.c s7_utilities.c
SOURCE_PATHS = $(SOURCES:%=src/%)
HEADERS = $(SOURCES:%.c=%.h)
HEADER_PATHS = $(HEADERS:%=src/%)
OBJECTS = $(SOURCES:%.c=%$(PLATFORM_OBJ_EXT))
OBJECT_PATHS = $(OBJECTS:%=src/%)

GLFW_linux = lib/glfw-LINUX/libglfw3.a
GLFW_windows = lib/glfw-WIN/libglfw3.a
GLFW_STATIC_LIB = $(GLFW_$(PLATFORM))
$(TARGET_PATH)/$(TARGET_NAME)$(TARGET_EXT): objects s7 glad
	mkdir -p bin
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ \
	$(OBJECT_PATHS) $(GLAD_OBJ) $(S7_OBJ) $(GLFW_STATIC_LIB) $(LDLIBS)

# src/s7_glfw.o: src/s7_glfw.c src/s7_glfw.h

objects: $(OBJECT_PATHS)
$(OBJECT_PATHS): $(SOURCE_PATHS) # $(HEADER_PATHS)


## Glad OpenGL Loader Library
GLAD_OBJ = lib/glad$(PLATFORM_OBJ_EXT)
glad: $(GLAD_OBJ)
GLAD_CFLAGS = -Iinclude
$(GLAD_OBJ): lib/glad.c include/glad/glad.h
	$(CC) $(GLAD_CFLAGS) $(LDFLAGS) -o $(GLAD_OBJ) -c lib/glad.c

glad-clean:
	$(RM) -r $(GLAD_OBJ)


## S7 Scheme Library
S7_OBJ = lib/s7/s7$(PLATFORM_OBJ_EXT)
S7_TAR = lib/s7.tar.gz
S7_SRC = lib/s7/s7.c include/s7/s7.h
s7: $(S7_OBJ)

S7_CFLAGS = -Iinclude/s7
$(S7_OBJ): $(S7_SRC)
	$(CC) $(S7_CFLAGS) -c lib/s7/s7.c -o $(S7_OBJ) $(CFLAGS_X_PLATFORM)

$(S7_SRC): $(S7_TAR)
	tar -xC lib -f $(S7_TAR)
	mkdir -p include/s7
	mv lib/s7/s7.h include/s7

$(S7_TAR):
	wget -O $(S7_TAR) https://ccrma.stanford.edu/software/s7/s7.tar.gz

s7-clean:
	$(RM) -r lib/s7 include/s7 $(S7_TAR)

print-vars:
	@echo platform: $(PLATFORM)
	@echo target: $(TARGET)
	@echo target after expansion: $($(TARGET))
	@echo target_path: $(TARGET_PATH)
	@echo cflags: $(CFLAGS)
	@echo ldflags: $(LDFLAGS)
	@echo ldlibs: $(LDLIBS)
	@echo glfw_static_lib: $(GLFW_STATIC_LIB)
	@echo sources: $(SOURCES)
	@echo source_paths: $(SOURCE_PATHS)
	@echo objects: $(OBJECTS)
	@echo object_paths: $(OBJECT_PATHS)

clean: mostly-clean s7-clean glad-clean

mostly-clean:
	$(RM) $(TARGET_PATH)/$(TARGET_NAME)$(TARGET_EXT) $(OBJECT_PATHS)
