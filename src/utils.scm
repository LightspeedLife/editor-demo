(define atom?
  (lambda (a)
    (not (pair? a))))

(define lat?
  (lambda (l)
    (cond
     ((null? l) #t)
     ((atom? (car l)) (lat? (cdr l)))
     (else #f))))

(define member?
  (lambda (m lat)
    (cond
     ((null? lat) #f)
     (else (or (eq? (car lat) m)
               (member? m (cdr lat)))))))

(define find
  (lambda (l item)
    (cond
     ((null? l) '())
     ((equal? (car l) item) (car l))
     (else (find (cdr l) item)))))

(define find-similar
  (lambda (l item)
    (cond
     ((null? l) '())
     ((equal? (car l) item) (car l))
     (else (find (cdr l) item)))))

(define rember
  (lambda (m lat)
    (cond
     ((null? lat) '())
     ((eq? (car lat) m) (cdr lat))
     (else (cons (car lat) (rember m (cdr lat)))))))

(define rember*
  (lambda (m a-list)
    (cond
     ((null? a-list) '())
     ((pair? (car a-list))
      (cons (rember* m (car a-list))
            (rember* m (cdr a-list))))
     ((eq? (car a-list) m) (cdr a-list))
     (else
      (cons (car a-list)
            (rember* m (cdr a-list)))))))

(define firsts
  (lambda (l)
    (cond
     ((null? l) '())
     ((pair? (car l))
      (cons (car (car l))
            (firsts (cdr l))))
     (else (firsts (cdr l))))))

(define insertR
  (lambda (new old lat)
    (cond
     ((null? lat) '())
     ((eq? (car lat) old)
      (cons (car lat)
            (cons new (cdr lat))))
     (else
      (cons (car lat)
            (insertR new old (cdr lat)))))))

(define insertR*
  (lambda (new old a-list)
    (cond
     ((null? a-list) '())
     ((and (atom? a-list)
           (eq? (car a-list) old))
      (cons (car a-list)
            (cons new (cdr a-list))))
     (else
      (cond
       ((pair? (car a-list))
        (cons (insertR* new old (car a-list))
              (insertR* new old (cdr a-list))))
       (else
        (cons (car a-list)
              (insertR* new old (cdr a-list)))))))))

(define multiinsertR*
  (lambda (new old a-list)
    (cond
     ((null? a-list) '())
     ((atom? (car a-list))
      (cond
       ((eq? (car a-list) old)
        (cons (car a-list)
              (cons new
                    (multiinsertR* new old (cdr a-list)))))
       (else
        (cons (car a-list)
              (multiinsertR* new old (cdr a-list))))))
     (else
      (cons (multiinsertR* new old (car a-list))
            (multiinsertR* new old (cdr a-list)))))))

(define insertL
  (lambda (new old lat)
    (cond
     ((null? lat) '())
     ((eq? (car lat) old)
      (cons new lat))
     (else
      (cons (car lat)
            (insertL new old (cdr lat)))))))

(define subst
  (lambda (new old lat)
    (cond
     ((null? lat) '())
     ((eq? (car lat) old)
      (cons new (cdr lat)))
     (else (cons (car lat)
                 (subst new old (cdr lat)))))))

(define multisubst
  (lambda (new old lat)
    (cond
     ((null? lat) '())
     ((eq? (car lat) old)
      (cons new (multisubst new old (cdr lat))))
     (else
      (cons (car lat)
            (multisubst new old (cdr lat)))))))

(define subst2
  (lambda (new old1 old2 lat)
    (cond
     ((null? lat) '())
     ((or (eq? (car lat) old1)
          (eq? (car lat) old2))
      (cons new (cdr lat)))
     (else
      (cons (car lat)
            (subst2 new old1 old2 (cdr lat)))))))

(define multirember
  (lambda (m lat)
    (cond
     ((null? lat) '())
     ((eq? (car lat) m)
      (multirember m (cdr lat)))
     (else
      (cons (car lat)
            (multirember m (cdr lat)))))))

(define multirember*
  (lambda (m a-list)
    (cond
     ((null? a-list) '())
     ((pair? (car a-list))
      (cons (multirember* m (car a-list))
            (multirember* m (cdr a-list))))
     ((eq? (car a-list) m)
      (multirember* m (cdr a-list)))
     (else
      (cons (car a-list)
            (multirember* m (cdr a-list)))))))

(define multiinsertR
  (lambda (new old lat)
    (cond
     ((null? lat) '())
     ((eq? (car lat) old)
      (cons (car lat)
            (cons new (multiinsertR new old (cdr lat)))))
     (else
      (cons (car lat)
            (multiinsertR new old (cdr lat)))))))

(define multiinsertL
  (lambda (new old lat)
    (cond
     ((null? lat) '())
     ((eq? (car lat) old)
      (cons new
            (cons (car lat)
                  (multiinsertL new old (cdr lat)))))
     (else
      (cons (car lat)
            (multiinsertL new old (cdr lat)))))))

;; Numbers

(define addtup
  (lambda (tup)
    (cond
     ((null? tup) 0)
     (else
      (+ (car tup) (addtup (cdr tup)))))))

(define tup+
  (lambda (tup1 tup2)
    (cond
     ((null? tup1) tup2)
     ((null? tup2) tup1)
     (else
      (cons (+ (car tup1) (car tup2))
            (tup+ (cdr tup1) (cdr tup2)))))))

(define gt?
  (lambda (a b)
    (cond
     ((zero? a) #f)
     ((zero? b) #t)
     (else (gt? (sub1 a) (sub1 b))))))

(define lt?
  (lambda (a b)
    (cond
     ((zero? b) #f)
     ((zero? a) #t)
     (else (lt? (sub1 a) (sub1 b))))))

(define ==
  (lambda (a b)
    (cond
     ((not (or (gt? a b) (lt? a b))) #t)
     (else #f))))

(define ^
  (lambda (a b)
    (cond
     ((== b 1) a)
     (else
      (x a (^ a (sub1 b)))))))

(define divide
  (lambda (sor end)
    (cond
     ((< sor end) 0)
     (else (add1 (divide (abs (- sor end)) end))))))

(define len
  (lambda (lat)
    (cond
     ((null? lat) 0)
     (else
      (add1 (len (cdr lat)))))))

(define pick
  (lambda (n lat)
    (cond
     ((null? lat) '())
     ((zero? n) '())
     ((zero? (sub1 n)) (car lat))
     (else
      (pick (sub1 n) (cdr lat))))))

(define rempick
  (lambda (n lat)
    (cond
     ((null? lat) '())
     ((zero? n) lat)
     ((one? n) (cdr lat))
     (else
      (cons (car lat)
            (rempick (sub1 n) (cdr lat)))))))

(define no-nums
  (lambda (lat)
    (cond
     ((null? lat) '())
     (else (cond
            ((number? (car lat))
             (no-nums (cdr lat)))
            (else
             (cons (car lat)
                   (no-nums (cdr lat)))))))))

(define all-nums
  (lambda (lat)
    (cond
     ((null? lat) '())
     (else (cond
            ((number? (car lat)) (cons (car lat)
                                       (all-nums (cdr lat))))
            (else
             (all-nums (cdr lat))))))))

(define eqan?
  (lambda (a b)
    (cond
     ((and (atom? a) (atom? b))
      (eq? a b))
     ((and (number? a) (number? b))
      (== a b))
     (else #f))))

(define occur
  (lambda (a lat)
    (cond
     ((null? lat) 0)
     (else (cond
            ((eqan? (car lat) a)
             (add1 (occur a (cdr lat))))
            (else
             (occur a (cdr lat))))))))

(define one?
  (lambda (n)
    (or (zero? (sub1 n)) #f)))

(define set?
  (lambda (lat)
    (cond
     ((null? lat) #t)
     ((member? (car lat) (cdr lat)) #f)
     (else (set? (cdr lat))))))

(define subset?
  (lambda (set-a set-b)
    (if (set? set-a)
        (cond
         ((null? set-a) #t)
         ((member? (car set-a) set-b)
          (subset? (cdr set-a) set-b))
         (else #f))
        #f)))

(define eqset?
  (lambda (set-a set-b)
    (and (subset? set-b set-a)
         (subset? set-a set-b))))

(define intersect
  (lambda (set-a set-b)
    (cond
     ((null? set-a) '())
     ((member? (car set-a) set-b)
      (cons (car set-a)
            (intersect (cdr set-a) set-b)))
     (else (interesect (cdr set-a) set-b)))))

(define union
  (lambda (set-a set-b)
    (cond
     ((null? set-a) '())
     ((not (member? (car set-a) set-b))
      (cons (car set-a)
            (union (cdr set-a) set-b)))
     (else (union (cdr set-a) set-b)))))

(define difference
  (lambda (set-a set-b)
    (cond
     ((null? set-a) '())
     ((member? (car set-a)) (difference (cdr set-a) set-b))
     (else (cons (car set-a)
                 (difference (cdr set-a) set-b))))))

(define intersect-all
  (lambda (l-set)
    (cond
     ((null? (cdr l-set)) (car l-set))
     (else (cons (intersect (car l-set)
                            (intersectall (cdr l-set))))))))

(define rember-f
  (lambda (test? m lat)
    (cond
     ((null? lat) '())
     ((test? (car lat) m) (cdr lat))
     (else (cons (car lat) (rember-f test? m (cdr lat)))))))

(define looking
  (lambda keep-looking ()))
