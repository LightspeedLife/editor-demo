/* s7_gui.c
 * Provides the basic GUI elements to s7.
 * This gives a schemer the ability to create / manage / destroy windows and
 * their contents from within s7. */
#include "s7_glfw.h"
#include <stdbool.h>            /* s7 includes this */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>             /* malloc / free */
#include <string.h>             /* obj->string methods */
#include <unistd.h>             /* sleep-testing */

#include <s7/s7.h>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>         /* necessary for opening windows */
#include <glad/glad.h>          /* and actually drawing anything. */

/* . Static Vars . *
 * _______________ */
static bool gl_loaded = 0;
static int glfw_window_tt = 0;

/* . Window . *
 * __________ */

static s7_pointer
glee_GLFWwindow_p_query(s7_scheme *sc, s7_pointer arg)
{
#define H_glfw_window_query "(glfw-window? window) returns #t if window is a GLFWwindow*."
	return s7_make_boolean
		(sc, s7_is_c_object(s7_car(arg))
		 && (glfw_window_tt == s7_c_object_type(s7_car(arg))));
}

static s7_pointer
glee_glfwCreateWindow(s7_scheme *sc, s7_pointer args)
{
#define H_glfw_create_window "(glfw_create_window name (width 640) (height 480) " \
		"creates a new window.  Be sure to retain the handle it returns."
	if (!s7_is_string(s7_car(args)))
		return s7_wrong_type_arg_error(sc, "glfw_create_window", 1, s7_car(args), "a string");

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	char const *window_name = s7_string(s7_car(args));
	GLFWwindow *window = glfwCreateWindow
		(s7_integer(s7_cadr(args)),  /* width */
		 s7_integer(s7_caddr(args)), /* height */
		 window_name,                /* title */
		 NULL, NULL);
	if (!window)
		fprintf(stderr, "Window \"%s\" failed to exist\n", s7_string(s7_car(args)));

	glfwMakeContextCurrent(window);

	/* gl must load once after we've made a window and set it as the current context */
	if (!gl_loaded) {
		if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
			fprintf(stderr, "GLAD: failed to initialize\n");
			exit(EXIT_FAILURE);
		}
		gl_loaded = 1;
	}
	return s7_make_c_object(sc, glfw_window_tt, window);
}

static s7_pointer
glee_glfwDestroyWindow(s7_scheme *sc, s7_pointer arg)
{
#define H_glfw_destroy_window "(glfw_destroy_window GLFWwindow*) destroys the given window"
	if (!glee_GLFWwindow_p_query(sc, arg))
		return s7_wrong_type_arg_error(sc, "glfw_destroy_window", 1, arg, "a glfw_window");

	GLFWwindow *window = s7_c_object_value(s7_car(arg));
	glfwDestroyWindow(window);
	return s7_nil(sc);
}

static s7_pointer
glee_glfwSwapBuffers(s7_scheme *sc, s7_pointer window)
{
	if (!glee_GLFWwindow_p_query(sc, window))
		return s7_wrong_type_arg_error(sc, "glfw_swap_buffers", 0, window, "a glfw window");
	glfwSwapBuffers(s7_c_object_value(s7_car(window)));
	return s7_nil(sc);
}

static s7_pointer
glee_glfwPollEvents(s7_scheme *sc, s7_pointer _)
{
	glfwPollEvents();
	return s7_nil(sc);
}

static s7_pointer
glee_glfwWindowShouldClose(s7_scheme *sc, s7_pointer window)
{
	if (!glee_GLFWwindow_p_query(sc, window))
		return s7_wrong_type_arg_error(sc, "glfw_swap_buffers", 0, window, "a glfw window");
	GLFWwindow *window_c = s7_c_object_value(s7_car(window));
	return s7_make_boolean(sc, GLFW_TRUE == glfwWindowShouldClose(window_c));
}

void
s7_glfw_init(s7_scheme *sc)
{
	glfw_window_tt = s7_make_c_type(sc, "GLFWwindow*");
	s7_c_type_set_gc_free(sc, glfw_window_tt, glee_glfwDestroyWindow);
	s7_define_function_star
		(sc, "glfwCreateWindow", glee_glfwCreateWindow,
		 "name (width 640) (height 480)", H_glfw_create_window);
	s7_define_function
		(sc, "glfwDestroyWindow", glee_glfwDestroyWindow,
		 1, 0, false, H_glfw_destroy_window);
	s7_define_function
		(sc, "GLFWwindow*?", glee_GLFWwindow_p_query,
		 1, 0, false, H_glfw_window_query);

	s7_define_function
		(sc, "glfwSwapBuffers", glee_glfwSwapBuffers,
		 1, 0, false, "swap buffers on arg");
	s7_define_function
		(sc, "glfwPollEvents", glee_glfwPollEvents,
		 0, 0, false, "poll events");
	s7_define_function
		(sc, "glfwWindowShouldClose", glee_glfwWindowShouldClose,
		 1, 0, false, "");

	return;
}
