#ifndef _S7_UTILITIES_H_
#define _S7_UTILITIES_H_
#include <s7.h>

void s7_utilities_init(s7_scheme *);

s7_pointer s7_bit_or(s7_scheme *, s7_pointer);
bool s7_check_args(s7_scheme *sc, s7_pointer args, s7_function *arg_arr, int count);

#endif /* _S7_UTILITIES_H_ */
