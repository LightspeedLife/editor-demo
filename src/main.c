#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#define GLFW_INCLUDE_NONE
#include <glad/glad.h>
#include <GLFW/glfw3.h>


#include "s7.h"
#include "s7_utilities.h"
#include "s7_glfw.h"
#include "s7_nuklear.h"

#include "buffer.h"

#if 0
struct sigaction new_act, old_act;

static void
handle_sigint(s7_scheme *sc, int ignored)
{
	fprintf(stderr, "interrupted!\n");
	s7_symbol_set_value(sc, s7_make_symbol(sc, "*interrupt*"), s7_make_continuation(sc));
	sigaction(SIGINT, &new_act, NULL);
	s7_quit(sc);
}
#endif

static void
error_cb(int e, char const *desc)
{ fprintf(stderr, "[GLFW]: Err (%d): %s\n", e, desc); }

int
main(int argc, char **argv)
{
	/* vel_init(vel_ctx); */
	struct buffer *buffer = buffer_create(2048);

	glfwSetErrorCallback(error_cb);

	if (!glfwInit()) {
		fprintf(stderr, "GLFW3: failed to initialize\n");
		exit(EXIT_FAILURE);
	}

	/* vel_load("main.scm"); */
	s7_scheme *sc = s7_init();
	s7_load(sc, "src/utils.scm");
	s7_utilities_init(sc);
	s7_glfw_init(sc);
	s7_nuklear_init(sc);
	/* s7_load(sc, "main.scm"); */

	printf("playing with scheme\nv0.0.00000000001 alpha");
	while (1) {
		fprintf(stdout, "\n> ");
		/* buffer_clear(buffer); */
		buffer_set_string(buffer, "(write ");
		clearerr(stdin);
		buffer_read(buffer, stdin);
		fprintf(stdout, "\n");
		if (0 == strcmp((char *)buffer_data(buffer), "(write "))
			buffer_append_string(buffer, "'crickets...");
		buffer_append_string(buffer, ")");
		s7_eval_c_string(sc, (char *)buffer_data(buffer));
	}

	/* while (!glfwWindowShouldClose(win1)) { */
	/* 	/\* glClear(GL_COLOR_BUFFER_BIT); *\/ */
        /*         /\* do stuff with Nuklear *\/ */

	/* 	glfwSwapBuffers(win1); */
	/* 	glfwPollEvents(); */
	/* } */

	puts("\n\nbye!");
	buffer_destroy(buffer);
	s7_free(sc);
	glfwTerminate();
	exit(0);
}
