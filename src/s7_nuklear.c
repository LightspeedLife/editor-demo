/* s7_nuklear.c
 *
 * exposes the nuklear library to s7
 * Nuklear provides lightweight WIMP primitives.
 */

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "s7.h"
#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_IMPLEMENTATION
#define NK_GLFW_GL3_IMPLEMENTATION
#define NK_KEYSTATE_BASED_INPUT
#include "nuklear.h"
#include "nuklear_glfw_gl3.h"

#include <stdlib.h>             /* malloc / free */

#include "s7_nuklear.h"

#include "s7_utilities.h"

/* Usage:
 *
 * (define (frame . draw-process)
 *   (letrec ((window (glfwCreateWindow ...)
 *            (glfw (new nk_glfw))
 *            (context (nk_glfw3_init glfw window))
 *            (draw-proc (when (not glfwWindowShouldClose window)
 *                         draw-process))))))
 * */

int            nk_glfw_tt    = 0;
int            nk_context_tt = 0;
int            nk_colorf_tt  = 0;
int            nk_vec2_tt    = 0;
int            nk_rect_tt    = 0;

/* . Utility Functions . *
 * _____________________ */
static s7_pointer
glee_nk_context_free(s7_scheme *sc, s7_pointer context)
{
	free(s7_c_object_value(context));
	return NULL;
}

static s7_pointer
glee_nk_rect(s7_scheme *sc, s7_pointer args)
{
#define H_nk_rect "(nk_rect x y w h)"
	{ s7_pointer arg_i, arg_ii = args;
		for (int i = 1; i <= 4; ++i) {
			/* if (s7_is_null(arg_ii)) */
			/* 	s7_wrong_number_of_args_error */
			/* 		(sc, "nk_rect", arg_ii); */
			arg_i = s7_car(arg_ii);
			if (!s7_integer(arg_i))
				s7_wrong_type_arg_error
					(sc, "nk_rect", i, arg_i, "a real");
			arg_ii = s7_cdr(arg_ii);
                }
	}
	/* s7_function arg_arr[] = { */
	/* 	s7_integer, */
	/* 	s7_integer, */
	/* 	s7_integer, */
	/* 	s7_integer */
	/* }; */
	/* s7_check_args(sc, args, arg_arr, 4); */
	struct nk_rect *rect = malloc(sizeof (*rect));
	*rect = nk_rect
		((float)s7_real(s7_car(args)),
		 (float)s7_real(s7_cadr(args)),
		 (float)s7_real(s7_caddr(args)),
		 (float)s7_real(s7_cadddr(args)));
	return s7_make_c_object(sc, nk_rect_tt, (void *)rect);
}

static s7_pointer
glee_nk_rect_free(s7_scheme *sc, s7_pointer rect)
{
	free(s7_c_object_value(rect));
	return NULL;
}

static s7_pointer
glee_nk_rect_to_string(s7_scheme *sc, s7_pointer rect)
{
	struct nk_rect *rect_c = s7_c_object_value(s7_car(rect));
	return s7_list
		(sc, 4,
		 s7_make_real(sc, rect_c->x),
		 s7_make_real(sc, rect_c->y),
		 s7_make_real(sc, rect_c->w),
		 s7_make_real(sc, rect_c->h));
}

static s7_pointer
glee_nk_colorf(s7_scheme *sc, s7_pointer args)
{
#define H_nk_colorf "(nk_colorf r g b a)"
	float             r     = (float)s7_real(s7_car(args));
	float             g     = (float)s7_real(s7_cadr(args));
	float             b     = (float)s7_real(s7_caddr(args));
	float             a     = (float)s7_real(s7_cadddr(args));
	struct nk_colorf *color = malloc(sizeof (*color));
	*color                  = (struct nk_colorf) { r, g, b, a };
	return s7_make_c_object(sc, nk_colorf_tt, (void *)color);
}

static s7_pointer
glee_nk_color_free(s7_scheme *sc, s7_pointer color)
{
	free(s7_c_object_value(color));
	return NULL;
}


static s7_pointer
glee_nk_vec2_free(s7_scheme *sc, s7_pointer vec2)
{
	free(s7_c_object_value(vec2));
	return NULL;
}

/* . Init Functions . *
 * __________________ */

static s7_pointer
new_nk_glfw(s7_scheme *sc, s7_pointer args)
{
	struct nk_glfw *glfw = malloc(sizeof (*glfw));
	return s7_make_c_object(sc, nk_glfw_tt, glfw);
}

static s7_pointer
free_nk_glfw(s7_scheme *sc, s7_pointer glfw3_obj)
{
	struct nk_glfw *glfw = s7_c_object_value(s7_car(glfw3_obj));
	free(glfw);
	return NULL;
}

static s7_pointer
glee_nk_glfw3_init(s7_scheme *sc, s7_pointer args)
{
#define H_glee_nk_init "(nk_init nk_glfw window)"
	struct nk_glfw    *glfw    = s7_c_object_value(s7_car(args));
	GLFWwindow        *window  = s7_c_object_value(s7_cadr(args));
	struct nk_context *context = nk_glfw3_init(glfw, window, NK_GLFW3_INSTALL_CALLBACKS);
	return s7_make_c_object(sc, nk_context_tt, (void *)context);
}

static s7_pointer
glee_nk_glfw3_font_stash(s7_scheme *sc, s7_pointer args)
{
#define nk_glfw3_font_stash "(nk_glfw3_font_stash glfw context font_path height)"
	struct nk_glfw       *glfw      = s7_c_object_value (s7_car    (args));
	struct nk_context    *ctx       = s7_c_object_value (s7_cadr   (args));
	s7_pointer            font_path = s7_caddr          (args);
	s7_double             height    = s7_real           (s7_cadddr (args));
	struct nk_font_atlas *atlas;
	struct nk_font       *font;

	nk_glfw3_font_stash_begin(glfw, &atlas);
	if (s7_is_string(font_path)) {
		font = nk_font_atlas_add_from_file
			(atlas, s7_string(font_path), height, 0);
		nk_style_set_font(ctx, &font->handle);
	} else return s7_wrong_type_arg_error
		       (sc, "nk_glfw3_font_stash", 3, font_path, "a string");
	nk_style_load_all_cursors(ctx, atlas->cursors);
	nk_glfw3_font_stash_end(glfw);

	return s7_nil(sc);
}

static s7_pointer
glee_nk_glfw3_shutdown(s7_scheme *sc, s7_pointer glfw)
{
	struct nk_glfw *glfw_c = s7_c_object_value(s7_car(glfw));
	nk_glfw3_shutdown(glfw_c);
	return s7_nil(sc);
}

/* . Drawing Functions . *
 * _____________________ */

static s7_pointer
glee_nk_glfw3_new_frame(s7_scheme *sc, s7_pointer arg)
{
#define H_nk_glfw3_new_frame "(nk_glfw3_new_frame nk-glfw) begins frame drawing"
	struct nk_glfw *glfw = s7_c_object_value(s7_car(arg));
	nk_glfw3_new_frame(glfw);
	return s7_nil(sc);
}

static s7_pointer
glee_nk_begin(s7_scheme *sc, s7_pointer args)
{
#define H_nk_begin "(nk_begin nk-context title bounds flags) begins a nuklear draw window"
	struct nk_context *context = s7_c_object_value(s7_car(args));
	char const        *title   = s7_string(s7_cadr(args));
	struct nk_rect    *bounds  = (struct nk_rect *)s7_c_object_value(s7_caddr(args));
	nk_flags           flags   = (nk_flags)s7_integer(s7_cadddr(args));
	return s7_make_boolean
		(sc, nk_true == nk_begin(context, title, *bounds, flags));
}

static s7_pointer
glee_nk_layout_row_static(s7_scheme *sc, s7_pointer args)
{
#define H_nk_layout_row_static "(nk_layout_row_static context, height, item-width, columns)"     \
		"Sets current row layout to fill columns number of widgets"                      \
		"in row with same item-width horizontal size. Once called all subsequent widget" \
		"calls greater than columns will allocate a new row with same layout."

		/* FIXME: check args
		   s7_wrong_type_arg_error(sc, "nk_layout_row_static"); */
	struct nk_context *context    = s7_c_object_value(s7_car(args));
	float              height     = (float)s7_real(s7_cadr(args));
	int                item_width = s7_integer(s7_caddr(args));
	int                columns    = s7_integer(s7_cadddr(args));
	nk_layout_row_static(context, height, item_width, columns);
	return s7_nil(sc);
}

static s7_pointer
glee_nk_button_label(s7_scheme *sc, s7_pointer args)
{
#define H_nk_button_label "(nk_button_label context title)"
	struct nk_context *context = s7_c_object_value(s7_car(args));
	char const        *title   = s7_string(s7_cadr(args));
	return s7_make_boolean
		(sc, nk_true == nk_button_label(context, title));
}

static s7_pointer
glee_nk_layout_row_dynamic(s7_scheme *sc, s7_pointer args)
{
#define H_nk_layout_row_dynamic "(H_nk_layout_row_dynamic context height columns)"          \
		"Sets current row layout to share horizontal space"                         \
		"between @cols number of widgets evenly. Once called all subsequent widget" \
		"calls greater than @cols will allocate a new row with same layout."

	struct nk_context *context = s7_c_object_value(s7_car(args));
	float              height  = (float)s7_real(s7_cadr(args));
	int                columns = (int)s7_integer(s7_caddr(args));
	nk_layout_row_dynamic(context, height, columns);
	return s7_nil(sc);
}

static s7_pointer
glee_nk_option_label(s7_scheme *sc, s7_pointer args)
{
#define H_nk_option_label "(nk_option_label context label t/f-active)"
	struct nk_context *context = s7_c_object_value(s7_car(args));
	char const        *label   = s7_string(s7_cadr(args));
	nk_bool            active  = (true == s7_boolean(sc, s7_caddr(args)))?
		                       nk_true : nk_false;
	return s7_make_boolean
		(sc, nk_true == nk_option_label(context, label, active));
}

static s7_pointer
glee_nk_propertyi(s7_scheme *sc, s7_pointer args)
{
#define H_nk_propertyi "(nk_propertyi context name minimum value" \
		" maximum step increment-per-pixel)"
	struct nk_context *context = s7_c_object_value (s7_car         (args));
	char const *name = s7_string                   (s7_cadr        (args));
	int minimum =                                  (int)s7_integer (s7_caddr  (args));
	int value =                                    (int)s7_integer (s7_cadddr (args));
	int maximum =                                  (int)s7_integer (s7_car    (s7_cddddr (args)));
	int step =                                     (int)s7_integer (s7_cadr   (s7_cddddr (args)));
	float increment_per_pixel =                    (float)s7_real  (s7_caddr  (s7_cddddr (args)));
	int result = nk_propertyi
		(context,
		 name,
		 minimum,
		 value,
		 maximum,
		 step,
		 increment_per_pixel);
	return s7_make_integer(sc, (int)result);
}

static s7_pointer
glee_nk_label(s7_scheme *sc, s7_pointer args)
{
#define H_nk_label "(nk_label context label alignment-flags)"
	struct nk_context *context = s7_c_object_value(s7_car(args));
	char const        *label   = s7_string(s7_cadr(args));
	nk_flags           flags   = (nk_flags)s7_integer(s7_caddr(args));
	nk_label(context, label, flags);
	return s7_nil(sc);
}

static s7_pointer
glee_nk_vec2(s7_scheme *sc, s7_pointer xy)
{
#define H_nk_vec2 "(nk_vec2 x y)"
	/* s7_function arg_arr[] = { */
	/* 	s7_is_real, */
	/* 	s7_is_real, */
	/* }; */

	/* s7_check_args(sc, xy, arg_arr, 2) */
	/* if (s7_is_null(sc, xy)) */
	/* 	s7_wrong_number_of_args_error(sc, "nk_vec2", xy); */
	if (!s7_is_real(s7_car(xy)))
		s7_wrong_type_arg_error(sc, "nk_vec2", 1, s7_car(xy), "a real");
	if (s7_is_null(sc, s7_cdr(xy)))
		s7_wrong_number_of_args_error(sc, "nk_vec2", s7_cdr(xy));
	if (!s7_is_real(s7_cadr(xy)))
		s7_wrong_type_arg_error(sc, "nk_vec2", 1, s7_cadr(xy), "a real");
	struct nk_vec2 *vec2 = malloc(sizeof (*vec2));
	*vec2 = (struct nk_vec2) { s7_real(s7_car(xy)),
	                           s7_real(s7_cadr(xy)) };
	return s7_make_c_object(sc, nk_vec2_tt, (void *)vec2);
}

static s7_pointer
glee_nk_combo_begin_color(s7_scheme *sc, s7_pointer args)
{
#define H_nk_combo_begin_color "(context color size)"
	struct nk_context *context = s7_c_object_value(s7_car(args));
	struct nk_colorf  *color   = s7_c_object_value(s7_cadr(args));
	struct nk_vec2     area    = nk_vec2(nk_widget_width(context), 400);
	return s7_make_boolean
		(sc, nk_true == nk_combo_begin_color
		 (context, nk_rgb_cf(*color), area));
}

static s7_pointer
glee_nk_color_picker(s7_scheme *sc, s7_pointer args)
{
	struct nk_context    *context   = s7_c_object_value(s7_car(args));
	struct nk_colorf     *color     = s7_c_object_value(s7_cadr(args));
	enum nk_color_format  format    = s7_integer(s7_caddr(args));
	struct nk_colorf     *new_color = malloc(sizeof (*new_color)); /* FIXME: ugly kludge */
	*new_color                      = nk_color_picker(context, *color, format);
	return s7_make_c_object
		(sc, nk_colorf_tt,
		 (void *)new_color);
}

static s7_pointer
glee_nk_combo_end(s7_scheme *sc, s7_pointer context)
{
	struct nk_context *context_c = s7_c_object_value(s7_car(context));
	nk_combo_end(context_c);
	return s7_nil(sc);
}

static s7_pointer
glee_nk_propertyf(s7_scheme *sc, s7_pointer args)
{
#define H_nk_propertyf "(nk_propertyf context name minimum value" \
		" maximum step increment-per-pixel)"
	struct nk_context *context = s7_c_object_value(s7_car(args));
	char const *name = s7_string (s7_cadr       (args));
	float minimum =              (float)s7_real (s7_caddr  (args));
	float value =                (float)s7_real (s7_cadddr (args));
	float maximum =              (float)s7_real (s7_car    (s7_cddddr (args)));
	float step =                 (float)s7_real (s7_cadr   (s7_cddddr (args)));
	float increment_per_pixel =  (float)s7_real (s7_caddr  (s7_cddddr (args)));
	float result = nk_propertyf
		(context,
		 name,
		 minimum,
		 value,
		 maximum,
		 step,
		 increment_per_pixel);
	return s7_make_real(sc, (double)result);
}

static s7_pointer
glee_nk_end(s7_scheme *sc, s7_pointer context)
{
	struct nk_context *context_c = s7_c_object_value(s7_car(context));
	nk_end(context_c);
	return s7_nil(sc);
}

static s7_pointer
glee_nk_glfw3_render(s7_scheme *sc, s7_pointer args)
{
	struct nk_glfw        *glfw             = s7_c_object_value (s7_car    (args));
	enum nk_anti_aliasing  aa               = s7_integer        (s7_cadr   (args));
	int                    max_vertx_buffer = s7_integer        (s7_caddr  (args));
	int                    max_lmnt_buffer  = s7_integer        (s7_cadddr (args));
	nk_glfw3_render(glfw, aa, max_vertx_buffer, max_lmnt_buffer);
	return s7_nil(sc);
}

void
s7_nuklear_init(s7_scheme *sc)
{
	/* .. nk types .. *
	 * ______________ */
	nk_colorf_tt = s7_make_c_type(sc, "nk_colorf_t");
	s7_c_type_set_gc_free(sc, nk_colorf_tt, glee_nk_color_free);
	s7_define_function(sc, "nk_colorf", glee_nk_colorf, 4, 0, false, H_nk_colorf);

	nk_context_tt = s7_make_c_type(sc, "nk_context_t");
	s7_c_type_set_gc_free(sc, nk_context_tt, glee_nk_context_free);

	s7_make_c_type(sc, "nk_rect_t");
	s7_c_type_set_gc_free(sc, nk_rect_tt, glee_nk_rect_free);
	s7_define_function(sc, "nk_rect", glee_nk_rect, 4, 0, false, H_nk_rect);

	s7_make_c_type(sc, "nk_vec2_t");
	s7_c_type_set_gc_free(sc, nk_vec2_tt, glee_nk_vec2_free);
	s7_define_function(sc, "nk_vec2", glee_nk_vec2, 2, 0, false, H_nk_vec2);

	nk_glfw_tt = s7_make_c_type(sc, "nk_glfw_t");
	s7_c_type_set_gc_free(sc, nk_glfw_tt, free_nk_glfw);
	s7_define_function(sc, "new_nk_glfw",       new_nk_glfw,            0, 0, false, "");
	s7_define_function(sc, "nk_glfw3_init",     glee_nk_glfw3_init,     2, 0, false, H_glee_nk_init);
	s7_define_function(sc, "nk_glfw3_shutdown", glee_nk_glfw3_shutdown, 1, 0, false, "");

	/* ... enum nk_color_format ... *
	 * ____________________________ */
	s7_define_constant(sc, "NK_RGB", s7_make_integer(sc, NK_RGB));
	s7_define_constant(sc, "NK_RGBA", s7_make_integer(sc, NK_RGBA));

	/* .. enum nk_panel_flags .. *
	 * ______________________ */
	s7_define_constant(sc, "NK_WINDOW_BORDER",      s7_make_integer(sc, NK_WINDOW_BORDER));
	s7_define_constant(sc, "NK_WINDOW_MOVABLE",     s7_make_integer(sc, NK_WINDOW_MOVABLE));
	s7_define_constant(sc, "NK_WINDOW_SCALABLE",    s7_make_integer(sc, NK_WINDOW_SCALABLE));
	s7_define_constant(sc, "NK_WINDOW_CLOSABLE",    s7_make_integer(sc, NK_WINDOW_CLOSABLE));
	s7_define_constant(sc, "NK_WINDOW_MINIMIZABLE", s7_make_integer(sc, NK_WINDOW_MINIMIZABLE));
	s7_define_constant(sc, "WINDOW_NO_SCROLLBAR",   s7_make_integer(sc, NK_WINDOW_NO_SCROLLBAR));
	s7_define_constant(sc, "NK_WINDOW_TITLE",       s7_make_integer(sc, NK_WINDOW_TITLE));
	s7_define_constant(sc, "SCROLL_AUTO_HIDE",      s7_make_integer(sc, NK_WINDOW_SCROLL_AUTO_HIDE));
	s7_define_constant(sc, "NK_WINDOW_BACKGROUND",  s7_make_integer(sc, NK_WINDOW_BACKGROUND));
	s7_define_constant(sc, "WINDOW_SCALE_LEFT",     s7_make_integer(sc, NK_WINDOW_SCALE_LEFT));
	s7_define_constant(sc, "WINDOW_NO_INPUT",       s7_make_integer(sc, NK_WINDOW_NO_INPUT));

	/* .. Text .. *
	 * __________ */

	/* ... enum nk_text_align ... *
	 * __________________________ */
	s7_define_constant(sc, "NK_TEXT_ALIGN_LEFT",     s7_make_integer(sc, NK_TEXT_ALIGN_LEFT));
	s7_define_constant(sc, "NK_TEXT_ALIGN_CENTERED", s7_make_integer(sc, NK_TEXT_ALIGN_CENTERED));
	s7_define_constant(sc, "NK_TEXT_ALIGN_RIGHT",    s7_make_integer(sc, NK_TEXT_ALIGN_RIGHT));
	s7_define_constant(sc, "NK_TEXT_ALIGN_TOP",      s7_make_integer(sc, NK_TEXT_ALIGN_TOP));
	s7_define_constant(sc, "NK_TEXT_ALIGN_MIDDLE",   s7_make_integer(sc, NK_TEXT_ALIGN_MIDDLE));
	s7_define_constant(sc, "NK_TEXT_ALIGN_BOTTOM",   s7_make_integer(sc, NK_TEXT_ALIGN_BOTTOM));

	/* ... enum nk_text_alignment ... *
	 * ______________________________ */
	s7_define_constant(sc, "NK_TEXT_LEFT",     s7_make_integer(sc, NK_TEXT_LEFT));
	s7_define_constant(sc, "NK_TEXT_CENTERED", s7_make_integer(sc, NK_TEXT_CENTERED));
	s7_define_constant(sc, "NK_TEXT_RIGHT",    s7_make_integer(sc, NK_TEXT_RIGHT));

	s7_define_function
		(sc, "nk_glfw3_font_stash", glee_nk_glfw3_font_stash,
		 3, 0, false, "");
	s7_define_function
		(sc, "nk_glfw3_new_frame", glee_nk_glfw3_new_frame,
		 1, 0, false, H_nk_glfw3_new_frame);
	s7_define_function
		(sc, "nk_begin", glee_nk_begin,
		 4, 0, false, H_nk_begin);
	s7_define_function
		(sc, "nk_layout_row_static", glee_nk_layout_row_static,
		 4, 0, false, H_nk_layout_row_static);
	s7_define_function
		(sc, "nk_layout_row_static", glee_nk_layout_row_static,
		 4, 0, false, H_nk_layout_row_static);
	s7_define_function
		(sc, "nk_layout_row_dynamic", glee_nk_layout_row_dynamic,
		 3, 0, false, H_nk_layout_row_dynamic);
	s7_define_function
		(sc, "nk_button_label", glee_nk_button_label,
		 2, 0, false, H_nk_button_label);
	s7_define_function
		(sc, "nk_option_label", glee_nk_option_label,
		 3, 0, false, H_nk_option_label);
	s7_define_function
		(sc, "nk_propertyi", glee_nk_propertyi,
		 7, 0, false, H_nk_propertyi);
	s7_define_function
		(sc, "nk_label", glee_nk_label,
		 3, 0, false, H_nk_label);
	s7_define_function
		(sc, "nk_combo_begin_color", glee_nk_combo_begin_color,
		 2, 0, false, H_nk_combo_begin_color);
	s7_define_function
		(sc, "nk_color_picker", glee_nk_color_picker,
		 3, 0, false, "");
	s7_define_function
		(sc, "nk_combo_end", glee_nk_combo_end,
		 1, 0, false, "");
}
