/* s7_gl.c
 *
 * s7 functions using OpenGL
 */
#include "s7_gl.h"
#include "s7.h"

static s7_pointer
glee_glClear(s7_scheme *sc, s7_pointer color_buffer_bit);
{
	glClear(s7_integer(s7_car(color_buffer_bit)));
	return s7_nil(sc);
}


static s7_pointer
glee_glClearColor(s7_scheme *sc, s7_pointer rgba)
{
	glClearColor
		(s7_real(s7_car(rgba))
		 s7_real(s7_cadr(rgba))
		 s7_real(s7_caddr(rgba))
		 s7_real(s7_cadddr(rgba)));
	return s7_nil(sc);
}

void
s7_gl_init(s7_scheme *sc)
{
	s7_define_constant(sc, "GL_COLOR_BUFFER_BIT", s7_make_integer(sc, GL_COLOR_BUFFER_BIT));

	s7_define_function(sc, "glClear", glee_glClear,
	                   1, 0, false, "");
	s7_define_function(sc, "glClearColor", glee_glClearColor,
	                   4, 0, false, "");
}
