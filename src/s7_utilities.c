#include "s7_utilities.h"
#include "s7.h"

s7_pointer
s7_bit_or(s7_scheme *sc, s7_pointer args)
{
	s7_pointer arg_i;
	s7_int bit_accum = 0x0;
	while (!s7_is_null(sc, args)) {
		arg_i = s7_car(args);
		bit_accum |= s7_integer(arg_i);
		args = s7_cdr(args);
	}
	return s7_make_integer(sc, bit_accum);
}

void
s7_utilities_init(s7_scheme *sc)
{
	s7_define_function(sc, "bit-or", s7_bit_or, 1, 0, true,
	                   "(bit-or first . rest) bitwise-ors each of the numbers in rest with first");
}

bool
s7_check_args(s7_scheme *sc, s7_pointer args, s7_function *arg_arr, int count)
{
	s7_pointer arg_i, arg_ii = args;
	for (int i = 1; i <= count; ++i) {
		if (s7_is_null(sc, arg_ii))
			s7_wrong_number_of_args_error
				(sc, "nk_rect", arg_ii);
		arg_i = s7_car(arg_ii);
		if (!(arg_arr[i])(sc, arg_i))
			s7_wrong_type_arg_error
				(sc, "nk_rect", i, arg_i, "a real");
		arg_ii = s7_cdr(arg_ii);
	}
	return true;
}
