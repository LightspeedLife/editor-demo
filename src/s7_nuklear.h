/* s7_nuklear.h
 *
 * nuklear lib for scheme */
#ifndef _S7_NUKLEAR_H
#define _S7_NUKLEAR_H
#include <s7.h>

void s7_nuklear_init(s7_scheme *sc);
#endif /* _S7_NUKLEAR_H */
