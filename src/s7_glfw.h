/* s7_gui.h
 * 
 * Expose GLFW to s7 */

#ifndef _S7_GUI_H_
#define _S7_GUI_H_
#include <s7/s7.h>

void s7_glfw_init(s7_scheme *);
#endif /* _S7_GUI_H_ */
