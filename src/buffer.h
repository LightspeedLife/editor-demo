/* buffer.h
 *
 * simple byte buffers */
#ifndef _BUFFER_H
#define _BUFFER_H
#include <stdint.h>             /* uintN_t */
#include <stdio.h>              /* FILE * */

struct buffer {
	uint32_t capacity;
	uint32_t consumed;      /* indexes 1-past the buffered data */
	uint8_t *data;
};

struct buffer *buffer_create(uint32_t const size);
void buffer_destroy(struct buffer *buffer);
void buffer_set(struct buffer *buffer, uint32_t const size, uint8_t const *data);
void buffer_set_string(struct buffer *buffer, char const *string);
void buffer_clear(struct buffer *buffer);
void buffer_append(struct buffer *buffer, uint32_t const size, uint8_t const *data);
void buffer_append_string(struct buffer *buffer, char const *data);
void buffer_read(struct buffer *buffer, FILE *restrict stream);
uint8_t *buffer_data(struct buffer *buffer);
uint32_t buffer_consumed(struct buffer *buffer);
int buffer_shrink(struct buffer *buffer, uint32_t const size);
#endif /* _BUFFER_H */
