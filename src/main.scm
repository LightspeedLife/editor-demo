(define w (glfwCreateWindow "mega big"))
(define g (new_nk_glfw))
(define c (nk_glfw3_init g w))
(nk_glfw3_font_stash g c "/usr/share/fonts/lekton/Lekton-Regular.ttf" 14)

(glfwWindowShouldClose w)

(nk_glfw3_new_frame g)

(nk_begin c "Demo" (nk_rect 50 50 230 250)
          (bit-or NK_WINDOW_BORDER NK_WINDOW_MOVABLE NK_WINDOW_SCALABLE 
                  NK_WINDOW_MINIMIZABLE NK_WINDOW_TITLE))

(list
 (nk_layout_row_static c 30 80 1)
 (nk_button_label c "button"))

(nk_layout_row_dynamic c 30 2)
(nk_option_label c "easy" #t)
(nk_option_label c "hard" #f)

(nk_layout_row_dynamic c 25 1)
(define comp 0)
(set! comp (nk_propertyi c "Compression:" 0 comp 100 10 1))

