/* buffer.c
 *
 * simple byte buffer implementation
 */
#include "buffer.h"

#include <stdlib.h>             /* malloc / free */
#include <string.h>             /* memcpy */

struct buffer *
buffer_create(uint32_t const capacity)
{
	struct buffer *buffer = malloc((sizeof (*buffer)));
	if (!buffer) {
		fprintf(stderr, "buffer: Allocation Error\n");
		exit(EXIT_FAILURE);
	}
	*buffer = (struct buffer) {
		.capacity = capacity,
		.consumed = 0,
		.data = malloc((capacity * sizeof *(buffer->data)))
	};
	if (!buffer->data) {
		fprintf(stderr, "buffer: Allocation Error\n");
		exit(EXIT_FAILURE);
	}
	return buffer;
}

void
buffer_destroy(struct buffer *buffer)
{
	free(buffer->data);
	free(buffer);
}

void
buffer_set(struct buffer *buffer, uint32_t const size, uint8_t const *data)
{
	if (1 + size > buffer->capacity) {
		uint8_t *temp = realloc(buffer->data, 2 * size);
		if (!temp) {
			fprintf(stderr, "buffer: Allocation Error\n");
			exit(EXIT_FAILURE);
		}
		buffer->data = temp;
		buffer->capacity = 2 * size;
	}
	memcpy(buffer->data, data, size);
	buffer->consumed = size;
	buffer->data[buffer->consumed] = '\0';
}

void
buffer_set_string(struct buffer *buffer, char const *string)
{ buffer_set(buffer, strlen(string), (uint8_t *)string); }

void
buffer_clear(struct buffer *buffer)
{ buffer->consumed = 0; }

void
buffer_append(struct buffer *buffer, uint32_t const size, uint8_t const *data)
{
	if (1 + size + buffer->consumed > buffer->capacity) {
		uint8_t *temp = realloc(buffer->data, 2 * (size + buffer->consumed));
		if (!temp) {
			fprintf(stderr, "buffer: Allocation error\n");
			exit(EXIT_FAILURE);
		}
		buffer->data = temp;
		buffer->capacity = 2 * (size + buffer->consumed);
	}
	memcpy(buffer->data + buffer->consumed, data, size);
	buffer->consumed += size;
	buffer->data[buffer->consumed] = '\0';
}

void
buffer_append_string(struct buffer *buffer, char const *string)
/* treat the current buffer position as a string, and append to it */
{ buffer_append(buffer, strlen(string), (uint8_t *)string); }



void
buffer_read(struct buffer *buffer, FILE *restrict stream)
{
	int ch;
	uint8_t temp_buf[256 << 8];
	uint32_t index = 0;
	while ((ch = fgetc(stream)) != EOF) {
		if ((256 << 8) == index) {
			buffer_append(buffer, index, temp_buf);
			index = 0;
		}
		temp_buf[index++] = ch;
	}
	buffer_append(buffer, index, temp_buf);
}

uint8_t *
buffer_data(struct buffer *buffer)
{ return buffer->data; }

uint32_t
buffer_consumed(struct buffer *buffer)
{ return buffer->consumed; }


int
buffer_shrink(struct buffer *buffer, uint32_t const size)
{
	if (size < buffer->consumed)
		return buffer->consumed;
	uint8_t *temp = NULL;
	if (0 < size) {
		temp = realloc(buffer->data, size);
		if (!temp) {
			fprintf(stderr, "buffer: Allocation error\n");
			exit(EXIT_FAILURE);
		}
		buffer->data = temp;
	} else {
		free(buffer->data);
		buffer->data = NULL;
	}
	buffer->capacity = size;
	return 0;
}
